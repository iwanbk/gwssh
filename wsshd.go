package main

import (
	"code.google.com/p/go.net/websocket"
	"fmt"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	fmt.Println("wsshd")
	r := mux.NewRouter()
	r.Handle("/wssh/{desthost}/{user}/{pass}", websocket.Handler(wsShell))
	err := http.ListenAndServe(":5000", r)
	if err != nil {
		fmt.Println("ListenAndServe:", err.Error())
	}
}

func wsShell(ws *websocket.Conn) {
	defer ws.Close()
	desthost := mux.Vars(ws.Request())["desthost"]
	user := mux.Vars(ws.Request())["user"]
	pass := mux.Vars(ws.Request())["pass"]
	log.Println("User : " + user + ":" + pass + " => " + desthost)

	cmdChan := make(chan string)
	respChan := make(chan string)
	shellChan := make(chan string)
	riChan := make(chan string)

	//start ssh client that act as a shell
	go shell(desthost, user, pass, cmdChan, respChan, shellChan)

	//this goroutine will forward response from ssh server to browser
	go forwardResponse(respChan, ws)

	go recvInput(cmdChan, ws, riChan)

	for {
		select {
		case s := <-shellChan:
			log.Println("message dari shell chan :", s)
		case s := <-riChan:
			log.Println("message dari ri chan :", s)
		}
	}

}
func recvInput(cmdChan chan string, ws *websocket.Conn, ctrlChan chan string) {
	var msg string
	for {
		err := websocket.Message.Receive(ws, &msg)
		if err != nil {
			log.Println("[main]read failed")
			websocket.Message.Send(ws, "Failed:"+err.Error())
			break
		}
		cmdChan <- msg
		resp := ""
		if msg == "\r" {
			resp, _ = buildResponse(msg + "\n")
		} else {
			resp, _ = buildResponse(msg)
		}
		websocket.Message.Send(ws, resp)
	}
	log.Println("[main]connection closed. Bye")
}

func forwardResponse(respCh chan string, ws *websocket.Conn) {
	for {
		resp := <-respCh
		js, _ := simplejson.NewJson([]byte("{}"))
		js.Set("data", resp)
		jbyte, err := js.MarshalJSON()
		if err != nil {
			log.Printf("json dumps err:%s\n", err)
		} else {
			websocket.Message.Send(ws, string(jbyte))
		}
	}
}
func buildResponse(r string) (string, error) {
	js, _ := simplejson.NewJson([]byte("{}"))
	js.Set("data", r)
	jbyte, err := js.MarshalJSON()
	if err != nil {
		log.Printf("json dumps err:%s\n", err)
		return "", err
	}
	return string(jbyte), nil
}
