// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// Modify by linuz.ly
package main

import (
	"bufio"
	"code.google.com/p/go.crypto/ssh"
	"log"
)

type clientPassword string

func (p clientPassword) Password(user string) (string, error) {
	return string(p), nil
}

type TerminalModes map[uint8]uint32

const (
	VINTR         = 1
	VQUIT         = 2
	VERASE        = 3
	VKILL         = 4
	VEOF          = 5
	VEOL          = 6
	VEOL2         = 7
	VSTART        = 8
	VSTOP         = 9
	VSUSP         = 10
	VDSUSP        = 11
	VREPRINT      = 12
	VWERASE       = 13
	VLNEXT        = 14
	VFLUSH        = 15
	VSWTCH        = 16
	VSTATUS       = 17
	VDISCARD      = 18
	IGNPAR        = 30
	PARMRK        = 31
	INPCK         = 32
	ISTRIP        = 33
	INLCR         = 34
	IGNCR         = 35
	ICRNL         = 36
	IUCLC         = 37
	IXON          = 38
	IXANY         = 39
	IXOFF         = 40
	IMAXBEL       = 41
	ISIG          = 50
	ICANON        = 51
	XCASE         = 52
	ECHO          = 53
	ECHOE         = 54
	ECHOK         = 55
	ECHONL        = 56
	NOFLSH        = 57
	TOSTOP        = 58
	IEXTEN        = 59
	ECHOCTL       = 60
	ECHOKE        = 61
	PENDIN        = 62
	OPOST         = 70
	OLCUC         = 71
	ONLCR         = 72
	OCRNL         = 73
	ONOCR         = 74
	ONLRET        = 75
	CS7           = 90
	CS8           = 91
	PARENB        = 92
	PARODD        = 93
	TTY_OP_ISPEED = 128
	TTY_OP_OSPEED = 129
)

func logPrint(user, server, msg string) {
	log.Printf("[%s@%s]%s\n", user, server, msg)
}
func shell(server, user, pass string, inpC, outC, ctrlChan chan string) {
	password := clientPassword(pass)
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.ClientAuth{
			// ClientAuthPassword wraps a ClientPassword implementation
			// in a type that implements ClientAuth.
			ssh.ClientAuthPassword(password),
		},
	}
	client, err := ssh.Dial("tcp", server, config)
	if err != nil {
		log.Println("Failed to dial: " + err.Error())
		return
	}
	defer client.Close()

	//create session
	logPrint(user, server, "Creating session...")
	session, err := createSession(client)
	if err != nil {
		log.Println("can't create session = ", err.Error())
	}
	defer session.Close()

	//create reader
	logPrint(user, server, "creating reader")
	reader, err := session.StdoutPipe()
	if err != nil {
		log.Println("Can't create reader:", err.Error())
		return
	}
	br := bufio.NewReader(reader)

	go handleResp(session, outC, br)

	//create writer
	logPrint(user, server, "creating writer")
	stdin, err := session.StdinPipe()
	if err != nil {
		log.Println("can't create stdin")
		return
	}
	wr := bufio.NewWriter(stdin)

	//create shell
	logPrint(user, server, "Create shell....")
	err = session.Shell()
	if err != nil {
		log.Println("Error creating shell:" + err.Error())
		return
	}

	for {
		s := <-inpC
		_, err := wr.Write([]byte(s))
		if err != nil {
			log.Println("failed to send command:", err.Error())
		}
		err = wr.Flush()
		if err != nil {
			log.Println("failed to flush command:", err.Error())
		}
	}
	return
}

func handleResp(ses *ssh.Session, respChan chan string, br *bufio.Reader) {
	for {
		//l, err := br.ReadString(byte('\r'))
		b, err := br.ReadByte()
		if err != nil {
			log.Println("err read from reader:", err.Error())
			break
		}
		respChan <- string(b)
		//respChan <- l
	}
}

// Create a session
func createSession(client *ssh.ClientConn) (*ssh.Session, error) {
	session, err := client.NewSession()
	if err != nil {
		log.Fatalf("unable to create session: %s", err)
		return nil, err
	}
	// Set up terminal modes
	modes := ssh.TerminalModes{
		ECHO:          0,     // disable echoing
		TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	}
	// Request pseudo terminal
	if err := session.RequestPty("xterm", 80, 40, modes); err != nil {
		log.Fatalf("request for pseudo terminal failed: %s", err)
		return nil, err
	}
	return session, nil
}
